class Monster{
  String  Name="";
  String  Type="";
  int Hp = 0;
  int Atk = 0;
  Monster(String Name,String Type , int Hp , int Atk){
    this.Name=Name;
    this.Type=Type;
    this.Hp=Hp;
    this.Atk=Atk;
  }
  String  getName(){
    return Name;
  }

  String  getType(){
    return Type;
  }
  int  getHp() {
    return Hp;
  }

  void setHp(int hp) {
    Hp = hp;
  }

  int  getAtk() {
    return Atk;
  }

  void setAtk(int atk) {
    Atk = atk;
  }
    void showMonster(){
    print("Name : $Name, Type : $Type, Hp : $Hp, Atk : $Atk");
  }
}