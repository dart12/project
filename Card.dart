import 'dart:io';
import 'dart:math';

class Card {
  final _random = new Random();
  List<int> rank = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
  List<String> suit = ["Diamond", "Clubs", "Spades", "Heart"];

  Card() {
    this.rank = rank as List<int>;
    this.suit = suit as List<String>;
  }


  int getRank(int index) {
    return rank[index];
  }

  String getSuit(int index) {
    return suit[index];
  }

  String randomSuit() {
    return suit[_random.nextInt(suit.length)];
  }

  int randomRank(){
    return rank[_random.nextInt(rank.length)];
  }
}
