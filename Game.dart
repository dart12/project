import 'Card.dart';
import 'Monster.dart';
import 'Character.dart';
import 'dart:io';

class Game {
  Card? card = null;
  int selectSide = 0;
  Character p1 = new Character("", 0, 0, 0, 0);
  Monster m1 = new Monster("", "", 0, 0);
  bool die = false;
  bool out = false;

  var thiefPic = (r'''
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣼⠟⢿⣷⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⣴⣤⡀⢠⡾⠃⠀⠀⠙⣿⣿⣦⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⢀⣿⣿⣿⣷⡟⠁⠀⠀⠀⠀⠘⣿⣿⣧⠀⠰⣦⣄⣀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⣿⣿⣿⣿⠀⠀⠀⣀⣀⣀⣤⣼⣿⣿⣷⠶⢿⡿⠛⠁⠀
⠀⠀⠀⠀⣠⣶⣾⣿⣿⣿⣿⣿⣿⣿⡿⠿⠟⠛⠛⠛⠋⢻⣿⣿⠀⠈⠀⠀⠀⠀
⠀⠀⠀⠀⠉⠛⠛⠛⠛⢻⣿⣿⣿⣿⡇⠀⠀⠀⠀⠀⠀⢸⣿⣿⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢿⣿⣿⣿⣇⠀⠀⠀⠀⠀⠀⠀⣿⣿⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢸⣿⣿⣿⣿⡿⣦⣄⡀⠀⠀⢰⣿⡏⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣿⣿⡿⢿⣿⣿⡄⠉⠻⢶⣤⣸⡿⠁⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⣸⣿⣿⠁⠀⠻⣿⣿⣆⠀⠀⠉⠛⠁⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⢀⣿⣿⠇⠀⠀⠀⠙⣿⣿⣆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⣸⣿⡿⠀⠀⠀⠀⠀⠈⢿⣿⣆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⢀⣿⣿⠁⠀⠀⠀⠀⠀⠀⠀⢻⣿⣆⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠈⠉⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠉⠉⠀⠀⠀
''');

  var warriorPic = (r'''
                        /|
                      /'||
                     |  ||         __.--._
                     |  ||      /~~   __.-~\ _
                     |  ||  _.-~ / _---._ ~-\/~\
                     |  || // /  /~/  .-  \  /~-\
                     |  ||((( /(/_(.-(-~~~~~-)_/ |
                     |  || ) (( |_.----~~~~~-._\ /
                     |  ||    ) |              \_|
                     |  ||     (| =-_   _.-=-  |~)        ,
                     |  ||      | `~~ |   ~~'  |/~-._-'/'/_,
                     |  ||       \    |        /~-.__---~ , ,
                     |  ||       |   ~-''     || `\_~~~----~
                     |  ||_.ssSS$$\ -====-   / )\_  ~~--~
             ___.----|~~~|%$$$$$$/ \_    _.-~ /' )$s._
    __---~-~~        |   |%%$$$$/ /  ~~~~   /'  /$$$$$$$s__
  /~       ~\    ============$$/ /        /'  /$$$$$$$$$$$SS-.
/'      ./\\\\\\_( ~---._(_))$/ /       /'  /$$$$%$$$$$~      \
(      //////////(~-(..___)/$/ /      /'  /$$%$$%$$$$'         \
 \    |||||||||||(~-(..___)$/ /  /  /'  /$$$%$$$%$$$            |
  `-__ \\\\\\\\\\\(-.(_____) /  / /'  /$$$$%$$$$$%$             |
      ~~""""""""""-\.(____) /   /'  /$$$$$%%$$$$$$\_            /
                    $|===|||  /'  /$$$$$$$%%%$$$$$( ~         ,'|
                __  $|===|%\/'  /$$$$$$$$$$$%%%%$$|        ,''  |
               ///\ $|===|/'  /$$$$$$%$$$$$$$%%%%$(            /'
                \///\|===|  /$$$$$$$$$%%$$$$$$%%%%$\_-._       |
                 `\//|===| /$$$$$$$$$$$%%%$$$$$$-~~~    ~      /
                   `\|-~~(~~-`$$$$$$$$$%%%///////._       ._  |
''');

  var babarossaPic = (r'''
                            ,-.
       ___,---.__          /'|`\          __,---,___
    ,-'    \`    `-.____,-'  |  `-.____,-'    //    `-.
  ,'        |           ~'\     /`~           |        `.
 /      ___//              `. ,'          ,  , \___      \
|    ,-'   `-.__   _         |        ,    __,-'   `-.    |
|   /          /\_  `   .    |    ,      _/\          \   |
\  |           \ \`-.___ \   |   / ___,-'/ /           |  /
 \  \           | `._   `\\  |  //'   _,' |           /  /
  `-.\         /'  _ `---'' , . ``---' _  `\         /,-'
     ``       /     \    ,='/ \`=.    /     \       ''
             |__   /|\_,--.,-.--,--._/|\   __|
             /  `./  \\`\ |  |  | /,//' \,'  \
            /   /     ||--+--|--+-/-|     \   \
           |   |     /'\_\_\ | /_/_/`\     |   |
            \   \__, \_     `~'     _/ .__/   /
             `-._,-'   `-._______,-'   `-._,-'
''');

  var harrpyPic = (r'''
            \                  /
    _________))                ((__________
   /.-------./\\    \    /    //\.--------.\
  //#######//##\\   ))  ((   //##\\########\\
 //#######//###((  ((    ))  ))###\\########\\
((#######((#####\\  \\  //  //#####))########))
 \##' `###\######\\  \)(/  //######/####' `##/
  )'    ``#)'  `##\`->oo<-'/##'  `(#''     `(
          (       ``\`..'/''       )
                     \""(
                      `- )
                      / /
                     ( /\
                     /\| \
                    (  \
                        )
                       /
                      (
''');
  var slimePic = (r'''
                                                                                                                                          
              ░░░░░░░░░░                                                                                                 
          ░░░░        ░░░░░░                                                                                         
        ░░                  ░░                                                                                     
      ░░                    ░░░░                                                                                 
    ░░                      ░░░░░░                           
    ░░                        ░░░░                          
  ░░                ░░    ░░  ░░░░░░                        
  ░░                ██░░  ██    ░░░░                        
  ░░                ██░░  ██    ░░░░            
  ░░            ░░            ░░░░░░             
  ░░░░░░                      ░░░░░░                                                       
    ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░              
        ░░░░░░░░░░░░░░░░░░░░░░                                                                                         
''');

  var gameName = (r'''
 ____    __    ___  ___    __    ____    __   ____    ____    __   ____  ____  __    ____ 
(  _ \  /__\  / __)/ __)  /__\  (  _ \  /__\ (_  _)  (  _ \  /__\ (_  _)(_  _)(  )  ( ___)
 ) _ < /(__)\( (__( (__  /(__)\  )   / /(__)\  )(     ) _ < /(__)\  )(    )(   )(__  )__) 
(____/(__)(__)\___)\___)(__)(__)(_)\_)(__)(__)(__)   (____/(__)(__)(__)  (__) (____)(____)
''');

  var heart1 = (r'''
 _______________
|               |
|  A            |
|     _   _     |
|    (  v  )    |
|     (   )     |
|      ( )      |
|       v       |
|               |
|             A |
|_______________|
''');

  var heart2 = (r'''
 _______________
|               |
|  2            |
|     _   _     |
|    (  v  )    |
|     (   )     |
|      ( )      |
|       v       |
|               |
|             2 |
|_______________|
''');

  var heart3 = (r'''
 _______________
|               |
|  3            |
|     _   _     |
|    (  v  )    |
|     (   )     |
|      ( )      |
|       v       |
|               |
|             3 |
|_______________|
''');

  var heart4 = (r'''
 _______________
|               |
|  4            |
|     _   _     |
|    (  v  )    |
|     (   )     |
|      ( )      |
|       v       |
|               |
|             4 |
|_______________|
''');

  var heart5 = (r'''
 _______________
|               |
|  5            |
|     _   _     |
|    (  v  )    |
|     (   )     |
|      ( )      |
|       v       |
|               |
|             5 |
|_______________|
''');

  var heart6 = (r'''
 _______________
|               |
|  6            |
|     _   _     |
|    (  v  )    |
|     (   )     |
|      ( )      |
|       v       |
|               |
|             6 |
|_______________|
''');

  var heart7 = (r'''
 _______________
|               |
|  7            |
|     _   _     |
|    (  v  )    |
|     (   )     |
|      ( )      |
|       v       |
|               |
|             7 |
|_______________|
''');

  var heart8 = (r'''
 _______________
|               |
|  8            |
|     _   _     |
|    (  v  )    |
|     (   )     |
|      ( )      |
|       v       |
|               |
|             8 |
|_______________|
''');

  var heart9 = (r'''
 _______________
|               |
|  9            |
|     _   _     |
|    (  v  )    |
|     (   )     |
|      ( )      |
|       v       |
|               |
|             9 |
|_______________|
''');

  var heart10 = (r'''
 _______________
|               |
|  10           |
|     _   _     |
|    (  v  )    |
|     (   )     |
|      ( )      |
|       v       |
|               |
|            10 |
|_______________|
''');

  var heart11 = (r'''
 _______________
|               |
|  J            |
|     _   _     |
|    (  v  )    |
|     (   )     |
|      ( )      |
|       v       |
|               |
|             J |
|_______________|
''');

  var heart12 = (r'''
 _______________
|               |
|  Q            |
|     _   _     |
|    (  v  )    |
|     (   )     |
|      ( )      |
|       v       |
|               |
|             Q |
|_______________|
''');

  var heart13 = (r'''
 _______________
|               |
|  K            |
|     _   _     |
|    (  v  )    |
|     (   )     |
|      ( )      |
|       v       |
|               |
|             K |
|_______________|
''');
  var clubs1 = (r'''
 _______________
|               |
|  A            |
|     _____     |
|    /     \    |
|   _\     /_   |
|  /         \  |
|  \___/ \___/  |
|      |_|      |
|             A |
|_______________|
''');
  var clubs2 = (r'''
 _______________
|               |
|  2            |
|     _____     |
|    /     \    |
|   _\     /_   |
|  /         \  |
|  \___/ \___/  |
|      |_|      |
|             2 |
|_______________|
''');
  var clubs3 = (r'''
 _______________
|               |
|  3            |
|     _____     |
|    /     \    |
|   _\     /_   |
|  /         \  |
|  \___/ \___/  |
|      |_|      |
|             3 |
|_______________|
''');
  var clubs4 = (r'''
 _______________
|               |
|  4            |
|     _____     |
|    /     \    |
|   _\     /_   |
|  /         \  |
|  \___/ \___/  |
|      |_|      |
|             4 |
|_______________|
''');
  var clubs5 = (r'''
 _______________
|               |
|  5            |
|     _____     |
|    /     \    |
|   _\     /_   |
|  /         \  |
|  \___/ \___/  |
|      |_|      |
|             5 |
|_______________|
''');
  var clubs6 = (r'''
 _______________
|               |
|  6            |
|     _____     |
|    /     \    |
|   _\     /_   |
|  /         \  |
|  \___/ \___/  |
|      |_|      |
|             6 |
|_______________|
''');
  var clubs7 = (r'''
 _______________
|               |
|  7            |
|     _____     |
|    /     \    |
|   _\     /_   |
|  /         \  |
|  \___/ \___/  |
|      |_|      |
|             7 |
|_______________|
''');
  var clubs8 = (r'''
 _______________
|               |
|  8            |
|     _____     |
|    /     \    |
|   _\     /_   |
|  /         \  |
|  \___/ \___/  |
|      |_|      |
|             8 |
|_______________|
''');
  var clubs9 = (r'''
 _______________
|               |
|  9            |
|     _____     |
|    /     \    |
|   _\     /_   |
|  /         \  |
|  \___/ \___/  |
|      |_|      |
|             9 |
|_______________|
''');
  var clubs10 = (r'''
 _______________
|               |
|  10           |
|     _____     |
|    /     \    |
|   _\     /_   |
|  /         \  |
|  \___/ \___/  |
|      |_|      |
|            10 |
|_______________|
''');
  var clubs11 = (r'''
 _______________
|               |
|  J            |
|     _____     |
|    /     \    |
|   _\     /_   |
|  /         \  |
|  \___/ \___/  |
|      |_|      |
|            J  |
|_______________|
''');
  var clubs12 = (r'''
 _______________
|               |
|  Q            |
|     _____     |
|    /     \    |
|   _\     /_   |
|  /         \  |
|  \___/ \___/  |
|      |_|      |
|            Q  |
|_______________|
''');
  var clubs13 = (r'''
 _______________
|               |
|  K            |
|     _____     |
|    /     \    |
|   _\     /_   |
|  /         \  |
|  \___/ \___/  |
|      |_|      |
|             K |
|_______________|
''');

  var Diamond1 = (r'''
 _______________
|               |
|  A            |
|       ^       |
|     /   \     |
|    /     \    |
|   (       )   |
|    \     /    |
|     \   /     |
|       v     A |
|_______________|
''');

  var Diamond2 = (r'''
 _______________
|               |
|  2            |
|       ^       |
|     /   \     |
|    /     \    |
|   (       )   |
|    \     /    |
|     \   /     |
|       v     2 |
|_______________|
''');

  var Diamond3 = (r'''
 _______________
|               |
|  3            |
|       ^       |
|     /   \     |
|    /     \    |
|   (       )   |
|    \     /    |
|     \   /     |
|       v     3 |
|_______________|
''');

  var Diamond4 = (r'''
 _______________
|               |
|  4            |
|       ^       |
|     /   \     |
|    /     \    |
|   (       )   |
|    \     /    |
|     \   /     |
|       v     4 |
|_______________|
''');

  var Diamond5 = (r'''
 _______________
|               |
|  5            |
|       ^       |
|     /   \     |
|    /     \    |
|   (       )   |
|    \     /    |
|     \   /     |
|       v     5 |
|_______________|
''');

  var Diamond6 = (r'''
 _______________
|               |
|  6            |
|       ^       |
|     /   \     |
|    /     \    |
|   (       )   |
|    \     /    |
|     \   /     |
|       v     6 |
|_______________|
''');

  var Diamond7 = (r'''
 _______________
|               |
|  7            |
|       ^       |
|     /   \     |
|    /     \    |
|   (       )   |
|    \     /    |
|     \   /     |
|       v     7 |
|_______________|
''');

  var Diamond8 = (r'''
 _______________
|               |
|  8            |
|       ^       |
|     /   \     |
|    /     \    |
|   (       )   |
|    \     /    |
|     \   /     |
|       v     8 |
|_______________|
''');

  var Diamond9 = (r'''
 _______________
|               |
|  9            |
|       ^       |
|     /   \     |
|    /     \    |
|   (       )   |
|    \     /    |
|     \   /     |
|       v     9 |
|_______________|
''');

  var Diamond10 = (r'''
 _______________
|               |
|  10           |
|       ^       |
|     /   \     |
|    /     \    |
|   (       )   |
|    \     /    |
|     \   /     |
|       v    10 |
|_______________|
''');

  var Diamond11 = (r'''
 _______________
|               |
|  J            |
|       ^       |
|     /   \     |
|    /     \    |
|   (       )   |
|    \     /    |
|     \   /     |
|       v     J |
|_______________|
''');

  var Diamond12 = (r'''
 _______________
|               |
|  Q            |
|       ^       |
|     /   \     |
|    /     \    |
|   (       )   |
|    \     /    |
|     \   /     |
|       v     Q |
|_______________|
''');

  var Diamond13 = (r'''
 _______________
|               |
|  K            |
|       ^       |
|     /   \     |
|    /     \    |
|   (       )   |
|    \     /    |
|     \   /     |
|       v     K |
|_______________|
''');

  var spades1 = (r'''
 _______________
|               |
|  A    ^       |
|      / \      |
|     (   )     |
|    (     )    |
|   (       )   |
|  (         )  |
| (___/| |\___) |
|      |_|    A |
|_______________|
''');

  var spades2 = (r'''
 _______________
|               |
|  2    ^       |
|      / \      |
|     (   )     |
|    (     )    |
|   (       )   |
|  (         )  |
| (___/| |\___) |
|      |_|    2 |
|_______________|
''');

  var spades3 = (r'''
 _______________
|               |
|  3    ^       |
|      / \      |
|     (   )     |
|    (     )    |
|   (       )   |
|  (         )  |
| (___/| |\___) |
|      |_|    3 |
|_______________|
''');

  var spades4 = (r'''
 _______________
|               |
|  4    ^       |
|      / \      |
|     (   )     |
|    (     )    |
|   (       )   |
|  (         )  |
| (___/| |\___) |
|      |_|    4 |
|_______________|
''');

  var spades5 = (r'''
 _______________
|               |
|  5    ^       |
|      / \      |
|     (   )     |
|    (     )    |
|   (       )   |
|  (         )  |
| (___/| |\___) |
|      |_|    5 |
|_______________|
''');

  var spades6 = (r'''
 _______________
|               |
|  6    ^       |
|      / \      |
|     (   )     |
|    (     )    |
|   (       )   |
|  (         )  |
| (___/| |\___) |
|      |_|    6 |
|_______________|
''');

  var spades7 = (r'''
 _______________
|               |
|  7    ^       |
|      / \      |
|     (   )     |
|    (     )    |
|   (       )   |
|  (         )  |
| (___/| |\___) |
|      |_|    7 |
|_______________|
''');

  var spades8 = (r'''
 _______________
|               |
|  8    ^       |
|      / \      |
|     (   )     |
|    (     )    |
|   (       )   |
|  (         )  |
| (___/| |\___) |
|      |_|    8 |
|_______________|
''');

  var spades9 = (r'''
 _______________
|               |
|  9    ^       |
|      / \      |
|     (   )     |
|    (     )    |
|   (       )   |
|  (         )  |
| (___/| |\___) |
|      |_|    9 |
|_______________|
''');

  var spades10 = (r'''
 _______________
|               |
| 10    ^       |
|      / \      |
|     (   )     |
|    (     )    |
|   (       )   |
|  (         )  |
| (___/| |\___) |
|      |_|   10 |
|_______________|
''');

  var spades11 = (r'''
 _______________
|               |
|  J    ^       |
|      / \      |
|     (   )     |
|    (     )    |
|   (       )   |
|  (         )  |
| (___/| |\___) |
|      |_|    J |
|_______________|
''');

  var spades12 = (r'''
 _______________
|               |
|  Q    ^       |
|      / \      |
|     (   )     |
|    (     )    |
|   (       )   |
|  (         )  |
| (___/| |\___) |
|      |_|    Q |
|_______________|
''');

  var spades13 = (r'''
 _______________
|               |
|  K    ^       |
|      / \      |
|     (   )     |
|    (     )    |
|   (       )   |
|  (         )  |
| (___/| |\___) |
|      |_|    K |
|_______________|
''');

  void printGameName() {
    int round = 7;
    for (int i = 0; i < round; i++) {
      print("\x1B[31m$gameName\x1B[0m");
      delay(100);
      reScreen();
      print("\x1B[32m$gameName\x1B[0m");
      delay(100);
      reScreen();
      print("\x1B[33m$gameName\x1B[0m");
      delay(100);
      reScreen();
      print("\x1B[35m$gameName\x1B[0m");
      delay(100);
      reScreen();
    }
    print(gameName);
  }

  void reScreen() {
    //refresh CommandLine
    print("\x1B[2J\x1B[0;0H");
  }

  void delay(int time) {
    //Sleep Function
    sleep(Duration(milliseconds: time));
  }

  String printCard(String suit, int index) {
    switch (suit) {
      case "Heart":
        return printCardHeart(index);

      case "Clubs":
        return printCardClubs(index);

      case "Diamond":
        return printCardDiamond(index);

      case "Spades":
        return printCardSpades(index);

      default:
        return "";
    }
  }

  String printCardHeart(int index) {
    switch (index) {
      case 1:
        return heart1;

      case 2:
        return heart2;

      case 3:
        return heart3;

      case 4:
        return heart4;

      case 5:
        return heart5;

      case 6:
        return heart6;

      case 7:
        return heart7;

      case 8:
        return heart8;

      case 9:
        return heart9;

      case 10:
        return heart10;

      case 11:
        return heart11;

      case 12:
        return heart12;

      case 13:
        return heart13;

      default:
        return "";
    }
  }

  String printCardClubs(int index) {
    switch (index) {
      case 1:
        return clubs1;

      case 2:
        return clubs2;

      case 3:
        return clubs3;

      case 4:
        return clubs4;

      case 5:
        return clubs5;

      case 6:
        return clubs6;

      case 7:
        return clubs7;

      case 8:
        return clubs8;

      case 9:
        return clubs9;

      case 10:
        return clubs10;

      case 11:
        return clubs11;

      case 12:
        return clubs12;

      case 13:
        return clubs13;

      default:
        return "";
    }
  }

  String printCardDiamond(int index) {
    switch (index) {
      case 1:
        return Diamond1;

      case 2:
        return Diamond2;

      case 3:
        return Diamond3;

      case 4:
        return Diamond4;

      case 5:
        return Diamond5;

      case 6:
        return Diamond6;

      case 7:
        return Diamond7;

      case 8:
        return Diamond8;

      case 9:
        return Diamond9;

      case 10:
        return Diamond10;

      case 11:
        return Diamond11;

      case 12:
        return Diamond12;

      case 13:
        return Diamond13;

      default:
        return "";
    }
  }

  String printCardSpades(int index) {
    switch (index) {
      case 1:
        return spades1;

      case 2:
        return spades2;

      case 3:
        return spades3;

      case 4:
        return spades4;

      case 5:
        return spades5;

      case 6:
        return spades6;

      case 7:
        return spades7;

      case 8:
        return spades8;

      case 9:
        return spades9;

      case 10:
        return spades10;

      case 11:
        return spades11;

      case 12:
        return spades12;

      case 13:
        return spades13;

      default:
        return "";
    }
  }

  bool Mon(int difficulty) {
    if (difficulty == 1) {
      m1 = new Monster("slime", "normal", 80, 40);
      print("\x1B[35m$slimePic\x1B[0m");
    } else if (difficulty == 2) {
      m1 = new Monster("Harppy", "normal", 160, 40);
      print("\x1B[31m$harrpyPic\x1B[0m");
    } else if (difficulty == 3) {
      m1 = new Monster("Babarossa", "Boss", 240, 50);
      print("\x1B[31m$babarossaPic\x1B[0m");
    } else {
      return true;
    }
    return false;
  }

  bool Job(int selectJob) {
    if (selectJob == 1) {
      p1 = new Character("Warrior", 200, 20, 1, 10);
      print("\x1B[31m$warriorPic\x1B[0m");
    } else if (selectJob == 2) {
      p1 = new Character("Thief", 150, 40, 1, 5);
      print("\x1B[31m$thiefPic\x1B[0m");
    } else {
      return true;
    }
    return false;
  }

//RanGame Is Main Programe
  void runGame() {
    printGameName();

    print(
        "Welcome to Baccarat Battle Select Your Job \n[1: Warrior] \n[2: Thef]");
    int selectJob = int.parse(stdin.readLineSync()!);
    while (Job(selectJob)) {
      print("Wrong press, Please select \n[1: Warrior] \n[2: Thef]");
      selectJob = int.parse(stdin.readLineSync()!);
    }
    while (out == false) {
      print(
          "Select Your Monster to Battle \n[1:slime] \n[2:Harppy] \n[3:Babarossa]");
      int difficulty = int.parse(stdin.readLineSync()!);
      while (Mon(difficulty)) {
        print(
            "Wrong press, Please select \n[1:slime] \n[2:Harppy] \n[3:Babarossa]");
        difficulty = int.parse(stdin.readLineSync()!);
        var maxHp = p1.getHp();
        var pLv = p1.getLv();
        maxHp = maxHp + ((pLv - 1) * 10);
        p1.setHp(maxHp);
      }
      m1.showMonster();

      delay(1000);
      p1.showCharacter();
      while (die == false) {
        reScreen();
        print(
            "Select Side You Want To Bet \n[1: PLayer] \n[2: Banker] \n[3: Draw]");
        selectSide = int.parse(stdin.readLineSync()!);
        int baccarat = process();
        bool win = checkSelectSide(selectSide, baccarat);

        var pHp = p1.getHp();
        var emHp = m1.getHp();

        var pAtk = p1.getAtk();
        var emAtk = p1.getAtk();

        var pArrmor = p1.getArrmor();

        int youwin = youWin(win);
        turn(youwin, pHp, emHp, pAtk, emAtk, pArrmor);
        pHp = p1.getHp();
        emHp = m1.getHp();
        print("\x1B[31m$pHp\x1B[0m");
        print(emHp);
        checkDie(pHp, emHp);
        delay(3000);
      }
      wantOut();
      p1.showCharacter();
      delay(2000);
    }
  }

  int youWin(bool win) {
    if (win == true) {
      print("=========== You Win ===========");
      return 1;
    } else {
      print("========== You Lose ==========");
      return 0;
    }
  }

  int randomCard() {
    Card card = new Card();

    int rank1 = card.randomRank();
    String suit1 = card.randomSuit();

    int rank2 = card.randomRank();
    String suit2 = card.randomSuit();

    String fristcard = printCard(suit1, rank1);
    String lastcard = printCard(suit2, rank2);
    showCardDouble(fristcard, lastcard);

    if (rank2 >= 10) {
      rank2 = 0;
    }
    var value2 = rank2;

    if (rank1 >= 10) {
      rank1 = 0;
    }
    var value1 = rank1;
    return (value1 + value2) % 10;
  }

  int hand() {
    int ans = randomCard();
    print(ans);
    return ans;
  }

  int cal() {
    print("========== Player Table ==========");
    delay(2000);
    int player = hand();
    print("========== Banker Table ==========");
    delay(1000);
    int banker = hand();
    int result = player - banker;
    return result;
  }

  int process() {
    int check = cal();
    if (check > 0) {
      print("Player Win");
      return 1;
    } else if (check == 0) {
      print("Draw");
      return 3;
    } else {
      print("Banker Win");
      return 2;
    }
  }

  bool checkSelectSide(int selectSide, int baccarat) {
    if (selectSide == baccarat) {
      return true;
    } else {
      return false;
    }
  }

  void showCardDouble(String frist, String last) {
    int k = 0;
    int j = 16;
    for (int i = 0; i < 11; i++) {
      var fristCard = frist.substring(k, j);
      var lastCard = last.substring(k, j);
      print("$fristCard$lastCard");
      k = j + 1;
      j = k + 17;
    }
  }

  void attacker(int emHp, int pAtk) {
    var nowHp = emHp - pAtk;
    m1.setHp(nowHp);
  }

  void defender(int pHp, int emAtk, int pArrmor) {
    var nowHp = pHp - (emAtk - pArrmor);
    p1.setHp(nowHp);
  }

  void turn(int win, int pHp, int emHp, int pAtk, int emAtk, int pArrmor) {
    if (win == 1) {
      attacker(emHp, pAtk);
    } else {
      defender(pHp, emAtk, pArrmor);
    }
  }

  void checkDie(int pHp, int emHp) {
    if (pHp <= 0 || emHp <= 0) {
      die = true;
      if (pHp == 0) {
        print("you die");
      } else {
        LvUp(p1);
      }
    }
  }

  void LvUp(Character p1) {
    p1.Hp += 10;
    p1.Atk += 10;
    p1.Lv += 1;
    p1.Arrmor += 5;
  }

  void wantOut() {
    print("Do you Want To out ? \n[Y : Yes] \n[N : no]");
    String wantout = stdin.readLineSync()!;
    if (wantout == "Y" || wantout == "y") {
      out = true;
    } else {
      reScreen();
      die = false;
    }
  }
}
