class Character {
  String Job = "";
  int Hp = 0;
  int Atk = 0;
  int Lv = 0;
  int Arrmor = 0;

  Character(String Job, int Hp, int Atk, int Lv, int Arrmor) {
    this.Job = Job;
    this.Hp = Hp;
    this.Atk = Atk;
    this.Lv = Lv;
    this.Arrmor = Arrmor;

  }
  String getJob() {
    return Job;
  }

  int getHp() {
    return Hp;
  }

  void setHp(int hp) {
    Hp = hp;
  }

  int getAtk() {
    return Atk;
  }

  void setAtk(int atk) {
    Atk = atk;
  }

  int getLv() {
    return Lv;
  }

  void setLv(int lv) {
    Lv = lv;
  }

  int getArrmor() {
    return Arrmor;
  }

  void setArrmor(int arrmor) {
    Arrmor = arrmor;
  }

  void showCharacter(){
    print("Job : $Job, Hp : $Hp, Atk : $Atk, Lv : $Lv, Arrmor : $Arrmor");
  }

  
}
